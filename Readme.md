# Boston Technologies VPN (Virtual Private Network) Deployment Script for Work From Home / Remote Workers

* For support on this product please e-mail hello@bostonnetworks.co.za or phone 021 949 8868.
* All remote hands-on support requires Anydesk (www.anydesk.com/download) so please install the product on your computer prior to call and provide the helpdesk with your support ID.


## Installation


1. On your Windows 10 desktop, right-click on the start button in the bottom left corner and select from the menu, "Windows Powershell (Admin)" (Alternatively press Windows Key + X)
2. The windows terminal will now appear on your desktop
3. Type in `set-executionpolicy bypass` and press enter
4. Select the following piece of code and copy it to your clipboard by using Control+C for Copy or right-click on the selected string and click "copy"
5. `(New-Object System.Net.WebClient).DownloadString("https://bitbucket.org/bn-konrad/vpnrepo/raw/9765157d42c65b9acd01681929cf91f205d03926/BN_PVE_VPN_Script.ps1") | powershell -command -`
6. Paste the string into the Powershell / Command line Terminal and press enter
7. The code will now generate a VPN and you will see the following output:

###

```
Name                  : VPN Peter Venter
ServerAddress         : 8c1a0*******.sn.mynetname.net
Name                  : VPN Peter Venter
ServerAddress         : 8c1a0*******.sn.mynetname.net
AllUserConnection     : True
Guid                  : {********-20A9-4BB2-BC35-A************}
TunnelType            : Pptp
AuthenticationMethod  : {MsChapv2}
EncryptionLevel       : Optional
L2tpIPsecAuth         :
UseWinlogonCredential : False
EapConfigXmlStream    :
ConnectionStatus      : Disconnected
RememberCredential    : True
SplitTunneling        : False
DnsSuffix             :
IdleDisconnectSeconds : 0
AllUserConnection     : True
Guid                  : {13F2232F-20A9-4BB2-BC35-ABFC5298106B}
TunnelType            : Pptp
AuthenticationMethod  : {MsChapv2}
EncryptionLevel       : Optional
L2tpIPsecAuth         :
UseWinlogonCredential : False
EapConfigXmlStream    :
ConnectionStatus      : Disconnected
RememberCredential    : True
SplitTunneling        : False
DnsSuffix             :
IdleDisconnectSeconds : 0

```



## Usage

1. On your Windows 10 taskbar at the bottom click your connectivity icon (either a Wi-Fi icon or Computer icon)
2. From the menu select `VPN Peter Venter` 
3. Enter your Peter Venter Domain username & password  (same login for your file shares)
	* Example:
	* Username: jaco (Not your e-mail address but company username)
	* Password: Password provided when you accessed your files for the first time
